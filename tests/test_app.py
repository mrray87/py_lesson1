import io
import sys
import tempfile

from app import (
    remove_special_characters,
    split_to_lower_words,
    count_matches,
    process_file,
    print_results
)

from collections import defaultdict


def test_remove_special_characters():
    contents = "T3st! *&^ Str-ing, isn't it?!"
    c_contents = remove_special_characters(contents)
    assert "3" not in c_contents
    assert "!" not in c_contents
    assert "&" not in c_contents
    assert "^" not in c_contents
    assert "-" in c_contents
    assert "'" in c_contents
    assert " " in c_contents


def test_split_to_words():
    content = "It's a test"
    c_content = split_to_lower_words(content)
    assert len(c_content) == 3
    assert "it's" in c_content


def test_count_matches():
    matches = defaultdict(int)
    content = ["it", "is", "what", "it", "is", "isnt't", "it"]
    count_matches(matches, content)
    assert len(matches) == 4
    assert matches["it"] == 3


def test_process_file_valid():
    with tempfile.NamedTemporaryFile(delete=False) as f:
        f.write(b"this is a test")
        f.seek(0)
        matches = process_file(f.name)
    assert matches["test"] == 1


def test_process_file_invalid_argument():
    captured_output = io.StringIO()
    sys.stdout = captured_output
    process_file("123")
    sys.stdout = sys.__stdout__
    assert "INVALID_INPUT" in captured_output.getvalue()


def test_process_file_invalid_argument_2():
    invalid_arg = 1.1
    captured_output = io.StringIO()
    sys.stdout = captured_output
    process_file(invalid_arg)
    sys.stdout = sys.__stdout__
    assert "INVALID_INPUT" in captured_output.getvalue()


def test_process_file_invalid_argument_3():
    captured_output = io.StringIO()
    sys.stdout = captured_output
    process_file("")
    sys.stdout = sys.__stdout__
    assert "INVALID_INPUT" in captured_output.getvalue()


def test_print_results_empty():
    matches = defaultdict(int)
    captured_output = io.StringIO()
    sys.stdout = captured_output
    print_results(matches)
    sys.stdout = sys.__stdout__
    assert "No words found." in captured_output.getvalue()
