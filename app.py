import sys
import re

from collections import defaultdict

UNEXPECTED_ERROR = "UNEXPECTED_ERROR"
INVALID_INPUT = "INVALID_INPUT"
ARGUMENT_ERROR = "ARGUMENT_MISSING"


def remove_special_characters(line):
    return re.sub(r"[^a-zA-Z-' ]", "", line)


def split_to_lower_words(line):
    return line.lower().split()


def count_matches(matches, words):
    for word in words:
        matches[word] += 1


def print_results(matches):
    print("Here are the results: ")
    if len(matches) > 0:
        for word, amount in matches.items():
            print("{} ({})".format(word, amount))
    else:
        print("No words found.")


def process_file(filename):
    matches = defaultdict(int)
    try:
        with open(filename) as f:
            for line in f:
                line_text = remove_special_characters(line)
                words = split_to_lower_words(line_text)
                count_matches(matches, words)
        return matches
    except Exception as e:
        print(INVALID_INPUT + ": %s", e)


if __name__ == '__main__':
    try:
        print_results(process_file(sys.argv[1]))
    except IndexError:
        print(ARGUMENT_ERROR)
